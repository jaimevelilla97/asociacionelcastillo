<?php 
$identity = $this->request->getAttribute('identity');
$isLoggedIn = isset($identity);

if ($isLoggedIn) {
    $usuario_rol = $identity->id_roles;
}
if ($usuario_rol != 6) {

    $this->assign('title', 'Editar actividad '); ?>

    <h1>Editar Actividad: <?= $actividad->nombre ?></h1>
    
    <?php
    
    echo $this->Form->create($actividad, ['context' => ['validator' => 'editar']]);
    
    echo $this->Form->controls(
        [
            'nombre' => [
                'label' => 'Nombre*'
            ],
            'fecha' => [
                'label' => 'Fecha de la actividad',
                'type' => 'date',
            ],
            'hora' => [
                'label' => 'Hora de comienzo',
                'type' => 'time',
                'step' => '0'
            ],
            'duracion' => [
                'label' => 'Duración de la actividad (en minutos)'
            ],
            'precio' => [
                'label' => 'Precio por participar'
            ],
            'email_contacto' => [
                'label' => 'Email de contacto'
            ],
            'telefono_contacto' => [
                'label' => 'Teléfono de contacto'
            ],
            'descripcion' => [ 
                'label' => 'Descripción de la actividad'
            ]
        ],
        [
            'legend' => 'Datos de la actividad',
        ]
    );
    
    echo $this->Html->link('Volver', ['controller' => 'actividades', 'action'=> 'index'], ['class' => 'button back-button']);
    
    echo $this->Form->button(__('Guardar cambios'));
    echo $this->Form->end();
}
else {
    echo $this->Html->link('Volver', ['controller' => 'bienvenida', 'action'=> 'index'], ['class' => 'button back-button']);?>
    <p>
    <img src="/webroot/img/error500.jpg"
        alt="NO AUTORIZADO" style="width:430px; height:500px; max-width:100%;">
<?php
}