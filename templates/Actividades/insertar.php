<?php $this->assign('title', 'Añadir nueva actividad'); ?>

<h1 class="esconder">Añadir Nueva Actividad</h1>

<?php
echo $this->Form->create($actividad, ['context' => ['validator' => 'insertar']]);

echo $this->Form->controls(
    [
        'nombre' => [
            'label' => 'Nombre*'
        ],
        'fecha' => [
            'label' => 'Fecha de la actividad',
            'format' => "dd/mm/yyyy",
            'type' => 'date'
        ],
        'hora' => [
            'label' => 'Hora de comienzo',
            'type' => 'time',
            'value' => '00:00',
            'step' => '0'
        ],
        'duracion' => [
            'label' => 'Duración de la actividad (en minutos)'
        ],
        'precio' => [
            'label' => 'Precio por participar'
        ],
        'email_contacto' => [
            'label' => 'Email de contacto'
        ],
        'telefono_contacto' => [
            'label' => 'Teléfono de contacto'
        ],
        'descripcion' => [ 
            'label' => 'Descripción de la actividad'
        ]
    ],
    [
        'legend' => 'Datos de la Actividad',
    ]
);

echo $this->Html->link('Volver', ['controller' => 'actividades', 'action'=> 'index'], ['class' => 'button back-button']);

echo $this->Form->button(__('Añadir actividad'));

echo $this->Form->end();

?>