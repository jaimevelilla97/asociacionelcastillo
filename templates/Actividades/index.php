<script type="text/javascript" src="//code.jquery.com/jquery-compat-git.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js" integrity="sha512-pdCVFUWsxl1A4g0uV6fyJ3nrnTGeWnZN2Tl/56j45UvZ1OMdm9CIbctuIHj+yBIRTUUyv6I9+OivXj4i0LPEYA==" crossorigin="anonymous"></script>

<?= $this->Html->script('exportarPDF.js') ?>

<h2><img style="width: 75px; padding-right: 10px;" src="/webroot/img/logo.jpg">Lista de Actividades</h2>

<ul id='paginator'>
    <?php if (!isset($search) || !$search): ?>
    <?= $this->Paginator->first('<i class="fa fa-fast-backward"></i> ' . __('1ª Pag.'), ['escape' => false]); ?>
    <?= $this->Paginator->prev('<i class="fa fa-backward"></i> ' . __('Anterior'), ['escape' => false]); ?>
    <?= $this->Paginator->numbers(); ?>
    <?= $this->Paginator->next('<i class="fa fa-forward"></i> ' . __('Siguiente'), ['escape' => false]); ?>
    <?= $this->Paginator->last('<i class="fa fa-fast-forward"></i> '. __('Última Pag.'), ['escape' => false]); ?>
    <?php endif ?>
</ul>
<?php 

$identity = $this->request->getAttribute('identity');
$isLoggedIn = isset($identity);
if ($isLoggedIn) {
    $usuario_rol = $identity->id_roles;
}
?>

<div id="top-options" style="width:100%; display:inline-block;" >
    <?php if($usuario_rol != 6)
    { 
        echo $this->Html->link(
            '<i class="fa fa-plus-square"></i> Añadir Actividad',
            ['action' => 'insertar'],
            ['escape' => false]
        );
    }?>
        <i class="fas fa-file-pdf"></i><a href="#" onclick="javascript:exportarPDF();">Exportar a PDF</a>
</div>

<table id="tbl">
    <thead class="cabecera">
        <tr>
            <th><?= $this->Paginator->sort('nombre', 'Nombre') ?></th>
            <th><?= $this->Paginator->sort('fecha', 'Fecha') ?></th>
            <th><?= $this->Paginator->sort('hora', 'Hora') ?></th>
            <th><?= $this->Paginator->sort('duracion', 'Duración') ?></th>
            <th>Precio</th>
            <th>Email Contacto</th>
            <th>Tlf. Contacto</th>
            <th>Descripcion</th>
            <?php if($usuario_rol != 6) { ?>
                <th id="eliminar">Acciones</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($actividades as $actividad): 
            
            $fecha = $actividad->fecha;
            if (!empty($fecha)) $fecha = $fecha->format("d/m/y");
            else $fecha = "Desconocida";
            
            ?>
        <tr>
            <td>
                <?= $actividad->nombre ?>
            </td>
            <td>
                <?= $fecha ?>
            </td>
            <td>
                <?= $actividad->hora->format('H:i') ?>
            </td>
            <td>
                <?= $actividad->duracion ?>
            </td>
            <td>
                <?= $actividad->precio ?>
            </td>
            <td>
                <?= $actividad->email_contacto ?>
            </td>
            <td>
                <?= $actividad->telefono_contacto ?>
            </td>
            <td>
                <?= $actividad->descripcion ?>
            </td>
            <?php if($usuario_rol != 6) { ?>
            <td headers="eliminar">
                <?=
                $this->Html->link(
                    '<i class="fa fa-edit"></i>', 
                    ['action' => 'editar', $actividad->id_actividades],
                    ['escape' => false]
                );
                ?>
                
                <?=
                $this->Form->postLink(
                    '<i class="fa fa-trash-alt"></i>',
                    ['action' => 'eliminar', $actividad->id_actividades],
                    ['confirm' => '¿Está seguro?', 'escape' => false]
                );
                ?>
            </td>
            <?php } ?>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>