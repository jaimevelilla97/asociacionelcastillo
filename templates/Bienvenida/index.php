<style>ul li b {font-size: 2.5rem;} </style>

<div class="principal clearfix">
<div>
    <h1 style="text-align:center; font-weight:bold;">JUNTA DIRECTIVA</h1>
        <img id="img-segura" style="float:left; display:inline-block;" width="670" height="520" src="/webroot/img/segura1.jpg">
        <ul class="junta" style="display:inline-block; padding-bottom:15%; padding-left:15%;">
            <li><b>Presidente:</b></li> <?= $presidente ?>
            <li><b>Vicepresidente: </b></li> <?= $vicepresidente ?>
            <li><b>Secretario: </b></li> <?= $secretario ?>
            <li><b>Tesorero: </b></li> <?= $tesorero ?>
            <li><b>Vocales: </b></li>
            <?php foreach ($vocales as $vocal) {
                echo $vocal;
            }  ?> 
        </ul>
    </div>
    
    <article class="art1">
        <h1 id="bajar" class="titulos">Disfrute de las mejores actividades en el pueblo para todas las edades </h1>
        <br>
        <img id="img-pilar" style="float:right;" width="670" height="520" src="webroot\img\pilar.jpg">
        <p class="texto">Todo empezó el verano de 1989 cuando un grupo de amigos comentamos con otras personas del pueblo nuestras inquietudes, (tener una zona recreativa, deportiva, sala de juegos biblioteca etc.)  para esparcimiento y disfrute nuestro y de nuestros hijos, ya que no había ninguna instalación  y el Ayuntamiento no tenía medios para llevarla acabo; los pocos recursos que tenía los estaba empleando en otras necesidades.  Tales manifestaciones fueron bien acogidas, pero de hay no paso, y no dejaron de ser simples conversaciones entre amigos, que al pasar el verano se fueron diluyendo y se olvidaron.
            Durante el resto del año fuimos madurando las ideas, las cuales sirvieron para que en el verano siguiente volviéramos a retomarlas de nuevo pero con más fuerza  al saber que teníamos un proyecto de lo que queríamos, y aunque al exponerlo, a algunos les parecía imposible y desorbitado, hoy nos están dando la razón.
            <br>En una mesa improvista se inició la toma de datos y nuestra sorpresa fue muy grata al ver la respuesta masiva, ya que en poco más de una hora se habían inscrito más de 100 socios que en días posteriores fue aumentando considerablemente, en la actualidad somos unos 360 socios  mayores de 20 años con lo que la asociación agrupa mas de 800 personas , puesto que los menores de 20 años por el momento no cuentan como socios.
            Además cuenta con una instalaciones deportivas, que tienen pista de baloncesto, de fútbol, frontón, parque infantil, barbacoas, y una revista que sale anualmente, “LINDAZOS”.
        </p>
    </article>  
    <article class="art2">
        <h1 class="titulos">Paisaje y tranquilidad a un click en el vídeo.</h1>      
        <br><div class="videoContainer" style="float:left;margin-bottom:10px;">
            <iframe id="videosegura" width="720" height="560" src="https://www.youtube.com/embed/vMaJes-9d-I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="max-width: 97%;"></iframe>
        </div> 
        <p class="texto">                                         
        Ya está instalada y funcionando la estación meteorológica adquirida por la Asociación El Castillo para poder consultar las condiciones meteorológicas actuales y archivadas en Segura de Baños. La instalación ha sido realizada en el exterior de la caseta del repetidor de TV, ya que de esta manera las medidas de los diferentes sensores van a ser totalmente fiables al no estar influenciados por elementos externos como paredes o vegetación colindante.
        En concreto, se trata del modelo HP1000 de la marca Froogit, que dispone de sensores para captar la temperatura, velocidad y dirección del viento, humedad, lluvia, radiación UV y luminosidad. Los datos de estos sensores están disponibles en la consola remota de la estación y mediante la instalación de una red informática de área local son recogidos por un miniordenador Raspberry, que con un software especializado de gestión meteorológica se encarga de guardar todos esos valores leídos en una base de datos y además cada 5 minutos realiza un volcado en la nueva página web para su visionado por cualquiera que lo desee. 
        Las funcionalidades de la nueva WEB van a ser la de poder consultar en tiempo real los datos actuales, para lo cual en la parte superior de la página se encuentra la fecha y la hora de la última actualización. En la misma página también se pueden acceder mediante menú a los resúmenes semanales, mensuales y anuales, con los cuales se podrá consultar por ejemplo la precipitación caída en el pueblo en el último mes, o año. En la parte inferior tendremos diversos mapas de la AEMET para su consulta on-line, así como también se dispone de los extractos NOAA correspondientes a los meses que lleve funcionando la estación.
        Esperamos que os sea de utilidad y si alguien tiene alguna sugerencia o alguna duda puede ponerse en contacto con nosotros para resolverlo.
        El link para acceder a la nueva web es: <a class="enlace" href src=" http://www.seguradebaños.com/meteo"> http://www.seguradebaños.com/meteo</a>
        </p>  
    </article>
    <article class="art3">
        <h1 class="titulos">La Virgen de la Aliaga</h1>      
        <div id="art3_1">
            <img id="img1-art3" style="max-width:100%;margin-right: 15px;" width="680" height="520" src="http://www.xn--seguradebaos-jhb.com/doc/Miguel%20Millan/Virgen%20Aliaga%202019/lindazos/FOTO3.JPG">
            <p class="texto">   
                No hace muchos años, si no recuerdo mal desde el 2004, los seguranos pasamos la ofrenda de flores a la Virgen del Pilar en un grupo llamado Segura de Baños. Todo ello,
                fruto de las peticiones de algunos de estos zahumaos que recorrían la ofrenda con otros grupos o en solitario. Por eso, se propuso a 
                la Asociación el Castillo, inscribirnos con este nombre, para comenzar otra tradición.
                Otro año más, sigue la tradición de la peregrinación de los seguranos hasta Cortes de Aragón, una caminata monte a través de 15km y 3 
                horas con parada organizada por nuestros voluntarios para repartir bocadillos, refrescos y alguna que otra cerveza para refrigerar a 
                los caminantes. Desde la Iglesia de Segura de Baños hasta el Santuario de Nuestra Señora de la Aliaga, niños, jóvenes y ancianos 
                caminan juntos pueden disfrutar de un día maravilloso juntos de celebración y concilio, siguiendo esta tradición que finaliza con 
                unas buenas parrilladas para comer los peñistas y amigos. 
            </p>
        </div>
        <div id="art3_2">
            <img id="img2-art3" style="max-width: 100%;margin-right:15px;" width="680" height="520" src="webroot\img\aliaga.jpg">
            <p class="texto">
                La fachada principal del Santuario de Nuestra Señora de la Aliaga, durante la Guerra Civil Española sufrió importantes desperfectos y perdió el remate de perfil mixtilíneo, las campanas y la imagen de la hornacina de la portada.
                La Cofradía de la Virgen de la Aliaga ha llevado a cabo la construcción de una sencilla espadaña donde se ha colocado la campana y en el  remate superior se ha instalado una cruz la cual es visible desde la lejanía. La campana es de bronce y la misma cuenta con dos inscripciones que hacen referencia, por un lado a la Cofradía de la Santísima Virgen de la Aliaga y por otro al año de su fundición.
                Al conjunto se añaden dos efigies, por una parte una cruz y en la otra la imagen de Nuestra Señora de la Aliaga.
                En la fachada principal del Santuario y en la hornacina que existía se ha colocado una talla de la Virgen de la Aliaga en alabastro de color blanco que contrasta con el dorado de la piedra arenisca de la fachada.
                La fiesta de la Virgen de la Aliaga siempre era para Segura de los Baños el segundo sábado de mayo pero este año la hemos compartido, por temas de calendario que marca la Cofradía, con el pueblo de Anadón y Cortes de Aragón.
                Como la tradición se debe continuar solamente nos quedamos todo el día en el Santuario los que vinimos de Segura.
            </p>
        </div>
    </article>
<div>