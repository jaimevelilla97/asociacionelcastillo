<?php
$identity = $this->request->getAttribute('identity');
$isLoggedIn = isset($identity);

if ($isLoggedIn) {
    $usuario_rol = $identity->id_roles;
}

if ($usuario_rol == 6) {
    echo $this->Html->link('Volver', ['controller' => 'bienvenida', 'action'=> 'index'], ['class' => 'button back-button']);
    ?>
    <p><img src="/webroot/img/error500.jpg"
        alt="NO AUTORIZADO" style="width:430px; height:500px; max-width:100%;">
    <?php
} else {
?>
<script type="text/javascript" src="//code.jquery.com/jquery-compat-git.js"></script>
<script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.2.61/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js" integrity="sha512-pdCVFUWsxl1A4g0uV6fyJ3nrnTGeWnZN2Tl/56j45UvZ1OMdm9CIbctuIHj+yBIRTUUyv6I9+OivXj4i0LPEYA==" crossorigin="anonymous"></script>

<?= $this->Html->script('exportarPDF.js') ?>

<h2><img style="width: 75px; padding-right: 10px;" src="/webroot/img/logo.jpg">Lista de Socios</h2>

<ul id='paginator'>
    <?php if (!isset($search) || !$search): ?>
    <?= $this->Paginator->first('<i class="fa fa-fast-backward"></i> ' . __('1ª Pag.'), ['escape' => false]); ?>
    <?= $this->Paginator->prev('<i class="fa fa-backward"></i> ' . __('Anterior'), ['escape' => false]); ?>
    <?= $this->Paginator->numbers(); ?>
    <?= $this->Paginator->next('<i class="fa fa-forward"></i> ' . __('Siguiente'), ['escape' => false]); ?>
    <?= $this->Paginator->last('<i class="fa fa-fast-forward"></i> '. __('Última Pag.'), ['escape' => false]); ?>
    <?php endif ?>
</ul>

<div id="top-options" style="width:100%; display:inline-block;" >
    <?= $this->Html->link(
            '<i class="fa fa-plus-square" aria-hidden="true"></i> Añadir Usuario',
            ['action' => 'insertar'],
            ['escape' => false]
        );
    ?>
    <?= $this->Html->link(
            '<i class="fa fa-search"></i> Buscar Usuario',
            ['action' => './search'],
            ['escape' => false]
        );
    ?>
    <i class="fas fa-file-pdf"></i>
    <a id="nomargin" href="#" onclick="javascript:exportarPDF();">Exportar a PDF</a>
</div>

<table id="tbl">
    <thead class="cabecera">    
        <tr>
            <th><?= $this->Paginator->sort('id', 'ID') ?></th>
            <th><?= $this->Paginator->sort('nombre', 'Nombre') ?></th>
            <th><?= $this->Paginator->sort('apellidos', 'Apellidos') ?></th>
            <th>DNI</th>
            <th>Email</th>
            <th>Cuota Pagada</th>
            <th id="eliminar">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($usuarios as $usuario): ?>
        <tr>
            <td>
                <?= $usuario->id_usuarios ?>
            </td>
            <td>
                <?= $usuario->nombre ?>
            </td>
            <td>
                <?= $usuario->apellidos ?>
            </td>
            <td>
                <?= $usuario->dni ?>
            </td>
            <td>
                <?= $usuario->email ?>
            </td>
            <td>
                <?= $usuario->cuota_pagada ?>
            </td>
            <td headers="eliminar">
            <?php 
                echo
                    $this->Html->link(
                        "<i class='fa fa-eye'></i>",
                        ['action' => 'ver', $usuario->id_usuarios],
                        ['escape' => false]
                    );
            if ($usuario->id_usuarios != 1) {
                echo
                    $this->Html->link(
                        '<i class="fa fa-edit"></i>', 
                        ['action' => 'editar', $usuario->id_usuarios],
                        ['escape' => false]
                    );
                echo
                    $this->Form->postLink(
                        '<i class="fa fa-trash-alt"></i>',
                        ['action' => 'eliminar', $usuario->id_usuarios],
                        ['confirm' => '¿Está seguro?', 'escape' => false]
                    );
            }?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php }