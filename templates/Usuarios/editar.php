<?php $this->assign('title', 'Editar usuario'); ?>

<h1>Editar Usuario: <?= $usuario->nombre . " " . $usuario->apellidos ?></h1>

<?php

echo $this->Form->create($usuario, ['context' => ['validator' => 'editar']]);
$opt = array(0 => "No", 1 => "Si");

echo $this->Form->controls(
    [
        'nombre' => [
            'label' => 'Nombre*'
        ],
        'apellidos' => [
            'label' => 'Apellidos*'
        ],
        'dni' => [
            'label' => 'DNI* (8 números 1 letra)',
            'pattern' => '^[0-9]{8}[A-Za-z]{1}$',
        ],
        'fecha_nacimiento' => [
            'label' => 'Fecha de nacimiento',
            'format' => 'dd/mm/yy'
        ],
        'email' => [
            'label' => 'E-mail'
        ],
        'telefono' => [
            'label' => 'Teléfono'
        ],
        'direccion' => [
            'label' => 'Dirección'
        ],
        'comunidad' => [
            'label' => 'Comunidad',
            'options' => $comunidades,
            'empty' => [0 => 'Seleccione su comunidad...'],
            'default' => [0],
            'disabled' => [0],
        ],
        'provincia_id' => [
            'label' => 'Provincia',
            'empty' => [0 => 'Seleccione su provincia...'],
            'default' => [0],
            'disabled' => [0],
        ],
        'id_roles' => [
            'label' => 'Cargo*',
            'options' => $roles,
            'empty' => [0 => 'Seleccione un rol...'],
            'default' => [6],
            'disabled' => [0],
        ],
        'cuota_pagada' => [
            'label' => 'Cuota Pagada',
            'options' => $opt,
            'empty' => [-1 => '¿Ha pagado?'],
            'default' => [-1],
            'disabled' => [-1],
        ]
    ],
    [
        'legend' => 'Datos personales',
    ]
);

echo $this->Form->controls(
    [
        'username' => [
            'label' => 'Username*'
        ],
        'password' => [
            'label' => 'Cambiar contraseña (Dejar vacío para mantener contraseña previa)',
            'type' => 'password',
            'value' => '',
        ],
    ],
    [
        'legend' => 'Nombre de usuario y contraseña',
    ]
);

echo $this->Html->link('Volver', ['controller' => 'usuarios', 'action'=> 'index'], ['class' => 'button back-button']);

//echo $this->Form->button(__('Guardar cambios'));
echo $this->Form->end();

?>