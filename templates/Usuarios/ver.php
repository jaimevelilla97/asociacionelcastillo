<?php $this->assign('title', "Ver: {$usuario->nombre}" . " " . "{$usuario->apellidos}"); ?>

<h1>Usuario: <?= $usuario->nombre . " " . $usuario->apellidos ?></h1>
<?php 
if ($usuario->id != 1) {
    $this->Html->link(
        '<i class="fa fa-edit"></i> Editar este usuario', 
        ['action' => 'editar', $usuario->id_usuarios],
        ['escape' => false]
);
}

$fecha_nacimiento = $usuario->fecha_nacimiento;
if (!empty($fecha_nacimiento)) $fecha_nacimiento = $fecha_nacimiento->format("d/m/y");
else $fecha_nacimiento = "Desconocida";
?>

<fieldset id="usuario">
    <legend>Datos Personales</legend>
    <p><b>Nombre:</b> <?= $usuario->nombre ?></p>
    <p><b>Apellidos:</b> <?= $usuario->apellidos ?></p>
    <p><b>Fecha de Nacimiento:</b> <?= $fecha_nacimiento ?></p>
    <p><b>Usuario:</b> <?= $usuario->username ?></p>
    <p><b>Rol:</b> <?= $usuario->id_roles ?></p>
    <p><b>Teléfono:</b> <?= $usuario->telefono ?></p>
    <p><b>E-mail:</b> <?= $usuario->email ?></p>
    <p><b>DNI:</b> <?= $usuario->dni ?></p>
</fieldset>

<fieldset id="domicilio">
    <legend>Domicilio</legend>
    <p><b>Dirección:</b> <?= $usuario->direccion ?></p>
    <p><b>Comunidad:</b> <?= $usuario->comunidad ?></p>
    <p><b>Provincia:</b> <?= $usuario->provincia ?></p>
</fieldset>

<fieldset id="cuotapagada">
    <legend>Cuota Anual</legend>
    <p><b>¿Cuota pagada?:</b> <?= $usuario->cuota_pagada ?></p>
</fieldset>

<fieldset id="username">
    <legend>Nombre de Usuario</legend>
    <p><b>Username:</b> <?= $usuario->username ?></p>
</fieldset>

<?=
$this->Html->link('Volver', ['controller' => 'usuarios', 'action'=> 'index'], ['class' => 'button back-button']);
?>