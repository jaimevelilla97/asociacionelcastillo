<?php

$identity = $this->request->getAttribute('identity');
$isLoggedIn = isset($identity);

if ($isLoggedIn) {
    $usuario_rol = $identity->id_roles;
}
?>
<?php if ($usuario_rol == 6): ?>
    <?= $this->Html->link(
        'Volver',
        [
            'controller' => 'bienvenida',
            'action'=> 'index'
        ], 
        [
            'class' => 'button back-button'
        ]
    ); ?>
    <p>
        <img src="/webroot/img/error500.jpg"
             alt="NO AUTORIZADO"
             style="width:430px; height:500px; max-width:100%;">
    </p>
<?php else: ?>
    <p><img src="/webroot/img/ganador.png" style="float:right;width:260px; height:260px; max-width:100%;"></p>
    <h1>Sorteo aleatorio de la Asociación "El Castillo".</h1>
    <h2 style='padding-bottom:3.5rem;'>¡FELICIDADES!</h2>
    <?= $anuncio; ?>
<?php endif; ?>
