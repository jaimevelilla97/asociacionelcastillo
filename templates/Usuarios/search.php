<style> 
    a:hover {
        cursor: pointer;
        background-color: black;
    }
    input:hover {
        background-color: lightblue;
    }
    #left-side{
        max-width: 45%;
        float: left;
    }
    #right-side{
        max-width: 45%;
        float: right;
    }
    footer{
        margin-top: 35em;
    }
</style>

<form action="/usuarios/search" style="max-width:600px;"  method="post">
    <div id="main">
        <div id="left-side">
            <input type="text" id="searchNombre" name="searchNombre" list="nombres" placeholder="Nombre..." />
            <input type="text" id="searchTelefono" name="searchTelefono" placeholder="Teléfono..." />
            <input type="text" id="searchEmail" name="searchEmail" placeholder="Email..." />
            <button type="submit" style="background-color:black;">Buscar  <i class="fa fa-search"></i></button>
        </div>

        <div id="right-side">
            <input type="text" id="searchApellidos" name="searchApellidos" list="apellidos" placeholder="Apellidos..." />
            <input type="text" id="searchDireccion" name="searchDireccion" placeholder="Direccion..." />
            <input type="text" id="searchDNI" name="searchDNI" placeholder="DNI..." />
            <?= $this->Html->link('Volver', ['controller' => 'usuarios', 'action'=> 'index'], ['class' => 'button back-button']); ?>
        </div>

        <datalist id="nombres">
            <?php foreach ($nombres as $nombre): ?>
                <option value="<?= $nombre ?>"><?= $nombre ?></option>
            <?php endforeach ?>
        </datalist>

        <datalist id="apellidos">
            <?php foreach ($apellidos as $apellido): ?>
                <option value="<?= $apellido ?>"><?= $apellido ?></option>
            <?php endforeach ?>
        </datalist>
    </div>
</form>