<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Http\Exception\NotFoundException;

$this->assign('title', "Bienvenid@");
$identity = $this->request->getAttribute('identity');

$usuario_nombre = $identity->nombre;
$usuario_apellidos = $identity->apellidos;

?>

<div align="center">
    <h1>Bienvenid@ <?= $usuario_nombre . " " . $usuario_apellidos ?></h1>
    <br>
</div>



