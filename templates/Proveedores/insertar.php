<?php $this->assign('title', 'Añadir nuevo proveedor'); ?>

<h1>Añadir Nuevo Proveedor</h1>

<?php
echo $this->Form->create($proveedor, ['context' => ['validator' => 'insertar']]);

echo $this->Form->controls(
    [
        'nombre_completo' => [
            'label' => 'Nombre*'
        ],
        'telefono' => [
            'label' => 'Teléfono',
        ],
        'email' => [
            'label' => 'Email',
        ],
        'web' => [
            'label' => 'Web de visita'
        ],
        'direccion' => [
            'label' => 'Dirección'
        ],
        'comentarios' => [ 
            'label' => 'Comentarios sobre el proveedor'
        ]
    ],
    [
        'legend' => 'Datos de Proveedores',
    ]
);

echo $this->Html->link('Volver', ['controller' => 'proveedores', 'action'=> 'index'], ['class' => 'button back-button']);

echo $this->Form->button(__('Añadir proveedor'));

echo $this->Form->end();

?>