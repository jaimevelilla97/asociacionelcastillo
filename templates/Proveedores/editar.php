<?php
$identity = $this->request->getAttribute('identity');
$isLoggedIn = isset($identity);

if ($isLoggedIn) {
    $usuario_rol = $identity->id_roles;
}

if ($usuario_rol == 6) {
    echo $this->Html->link('Volver', ['controller' => 'bienvenida', 'action'=> 'index'], ['class' => 'button back-button']);
    ?>
    <p><img src="/webroot/img/error500.jpg"
        alt="NO AUTORIZADO" style="width:430px; height:500px; max-width:100%;">
    <?php
} else {
?>
<?php $this->assign('title', 'Editar proveedor'); ?>

<h1>Editar Proveedor:  <?= $proveedor->nombre_completo ?> </h1>

<?php
echo $this->Form->create($proveedor, ['context' => ['validator' => 'editar']]);

echo $this->Form->controls(
    [
        'nombre_completo' => [
            'label' => 'Nombre*'
        ],
        'telefono' => [
            'label' => 'Teléfono',
        ],
        'email' => [
            'label' => 'Email',
        ],
        'web' => [
            'label' => 'Web de visita'
        ],
        'direccion' => [
            'label' => 'Dirección'
        ],
        'comentarios' => [ 
            'label' => 'Comentarios sobre el proveedor'
        ]
    ],
    [
        'legend' => 'Datos de Proveedores',
    ]
);

echo $this->Html->link('Volver', ['controller' => 'proveedores', 'action'=> 'index'], ['class' => 'button back-button']);

echo $this->Form->button(__('Guardar Cambios'));

echo $this->Form->end();

}?>