<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$appName = 'AsociacionElCastillo';

$identity = $this->request->getAttribute('identity');
$isLoggedIn = isset($identity);

if ($isLoggedIn) {
    $usuario_nombre = $identity->nombre;
    $usuario_apellidos = $identity->apellidos;
    $usuario_rol = $identity->id_roles;
}

?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $appName ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.1/normalize.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


    <?= $this->Html->css('milligram.min.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('asociacion.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.min.js" integrity="sha512-YSdqvJoZr83hj76AIVdOcvLWYMWzy6sJyIMic2aQz5kh2bPTd9dzY3NtdeEAzPp/PhgZqr4aJObB3ym/vsItMg==" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.9.2/html2pdf.bundle.min.js" integrity="sha512-pdCVFUWsxl1A4g0uV6fyJ3nrnTGeWnZN2Tl/56j45UvZ1OMdm9CIbctuIHj+yBIRTUUyv6I9+OivXj4i0LPEYA==" crossorigin="anonymous"></script>

    <?= $this->Html->script('ubicaciones.js') ?>
</head>

<body>
    <header class="top-nav">
        <div class="top-nav-title">
            <?php $img = '/webroot/img/82x150.png'; ?>
            <div class="logosegura">
                <img id="escudosegura" src="<?= $img ?>" alt="asociacionElCastillo" width="20%" height="20%">
            </div>
            <div class="tituloplataforma">
                <span class="titulo">Asociación Cultural "El Castillo"</span>
                <span class="subtitulo">Plataforma de Gestión</span>
            </div>
            <?php if ($isLoggedIn): ?>
                <?php if ($usuario_rol != 6): ?>
                    <ul id="links">
                        <li class="top-nav-input">
                            <i class="fa fa-building"></i> 
                            <a href="/bienvenida">Página Principal</a>
                        </li>
                        <li class="top-nav-input">
                            <i class="fa fa-user"></i> 
                            <a href="/usuarios">Usuarios</a>
                        </li>

                        <li class="top-nav-input">
                            <i class="fas fa-beer"></i>
                            <a href="/actividades">Actividades</a>
                        </li>

                        <li class="top-nav-input">
                            <i class="fa fa-address-book"></i>
                            <a href="/proveedores">Proveedores</a>
                        </li>
                        <li class="top-nav-input">
                            <i class="fas fa-dollar-sign"></i>
                            <a href="/usuarios/sortear">Sorteo</a>
                        </li>
                    </ul>
                <?php endif; ?>
                <?php if ($usuario_rol === 6): ?>
                    <ul id="links">
                        <li class="top-nav-input">
                            <i class="fa fa-building"></i> 
                            <a href="/bienvenida">Página Principal</a>
                        </li>
                        <li class="top-nav-input">
                            <i class="fa fa-user"></i>
                            <a href="/actividades">Actividades</a>
                        </li>
                    </ul>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php if ($isLoggedIn): ?>
            <div class="top-nav-title"></div>
            <div class="top-nav-links">
                <p><b>
                &nbsp;<i class="fa fa-user"></i>&nbsp;
                <?php 
                if (isset($usuario_nombre) && isset($usuario_apellidos)) 
                    echo "{$usuario_nombre} {$usuario_apellidos}";
                ?>
                </b></p>
                <p><i class="fa fa-sign-out-alt green"></i>&nbsp;<a href="/usuarios/logout">Cerrar sesión</a></p>
                <p><i class="fa fa-question-circle yellow"></i>&nbsp;<a href="mailto:ac.elcastillo@seguradebanos.com">Contacto</a></p>
                <p><i class="fa fa-mountain maroon"></i>&nbsp;<a href="http://www.xn--seguradebaos-jhb.com">Página "Segura de Baños"</a></p>
                <p><i class="fab fa-facebook-square blue"></i>&nbsp;<a href="https://www.facebook.com/AsociacionElCastillo/">Facebook "El Castillo"</a></p>

            </div>
        <?php endif; ?>

    </header>
    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer style="font-size:0.6em;">
        <hr>Copyright © 2005 - 2020 Segura de los Baños. Diseñado por la Asociación Cultural EL
        CASTILLO www.seguradebaños.com es un dominio titularidad de La Asociación Cultural El Castillo con C.I.F. 
        nº G-44.110.823 y domicilio a estos efectos en Plaza Mayor s/n 44793 SEGURA DE BAÑOS (TERUEL). Los contenidos, 
        organización y elección de enlaces han sido seleccionados y coordinados por La Asociación Cultural El Castillo.
        Se prohíbe la reproducción total o parcial de los contenidos de esta web sin previa autorización por escrito de La 
        Asociación Cultural El Castillo.
    </footer>
</body>
</html>