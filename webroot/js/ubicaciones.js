$(function() {

    $('#comunidad, #comunidad-id').on('change', function() {
        var id = $(this).val();
        var csrf = $('input[name="_csrfToken"]').val(); 
        var targeturl = '/provincias/getProvinciasByComunidad';
        
        //$("#divLoading").addClass('show');
        //$('#state').html(`<option value="-1">Select State</option>`);
        //console.log(csrf); 	
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: targeturl,                  
            data: {
                'id': id,
                '_csrfToken': csrf,
            },
            headers: { 'X-XSRF-TOKEN' : csrf },
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', csrf);
            },
            success: function(result) {
                var provincias = result.provincias;

                $('#provincia-id').html("");
                $('#provincia-id').append("<option value='0' selected disabled>Seleccione una provincia...</option>");
                provincias.forEach(function(elem) {
                    $('#provincia-id').append("<option value='" + elem.id + "'>" + elem.nombre + "</option>");
                });
            }
        });	
        
    });

});