function exportarPDF() {

    // clonar la tabla para no cargarnos columnas de la original en la web
    var tabla_pdf = document.getElementById('tbl').cloneNode(true);
   
    // Quitar la columna de Acciones en el clon a imprimir como PDF
    var th_eliminado = tabla_pdf.querySelector("th#eliminar");
    if (th_eliminado != null) {
    th_eliminado.parentNode.removeChild(th_eliminado);
        var tds_informe_pdf = tabla_pdf.querySelectorAll("td[headers='eliminar']");
        for (td of tds_informe_pdf) {
            td.parentNode.removeChild(td);
        }
    }

    var opt = {
        margin: 0.25,
        // margin: [0, 0.25],
        // margin: [0.25, 0.25, 0.5, 0.25],
        filename: 'ListadoAsociacion.pdf',
        pagebreak: {
            mode: 'avoid-all',
            after: 'table:not(:last-child)',
            avoid: 'table:last-child',
        },
        image: { 
            type: 'jpeg',
            quality: 0.75
        },
        enableLinks: false,
        html2canvas: {
            // scale: 2, // scale 2 breaks Firefox
            // dpi: 192,
        },
        jsPDF: { 
            unit: 'in',
            orientation: 'l',
            compress: true,
        }
    };
    tabla_pdf.style.wordWrap = 'break-word';
    
    console.log(tabla_pdf);
    html2pdf().set(opt).from(tabla_pdf).save().then(function() {
        console.log("**__**");
    });
}