<?php
namespace App\Policy;

use App\Model\Entity\Usuario;
use Authorization\IdentityInterface;

class UsuarioPolicy
{
    public function canVer(IdentityInterface $usuario, Usuario $usuarioAVer)
    {
        return $this->admin($usuario);
    }

    public function canInsertar(IdentityInterface $usuario, Usuario $usuarioAInsertar)
    {
        return $this->admin($usuario);
    }

    public function canEditar(IdentityInterface $usuario, Usuario $usuarioAEditar)
    {
        // aseguramos un superusuario id=1 que no pueda editarse ni eliminarse para no quedarnos sin acceso a la plataforma
        return $this->admin($usuario) && $usuarioAEditar->id_usuarios != 1;
    }
    
    public function canEliminar(IdentityInterface $usuario, Usuario $usuarioAEliminar)
    {
        // aseguramos un superusuario id=1 que no pueda editarse ni eliminarse para no quedarnos sin acceso a la plataforma
        return $this->admin($usuario) && $usuarioAEliminar->id_usuarios != 1;
    }
     
    protected function admin(IdentityInterface $usuario)
    {
        // id_roles 1-5 son los cargos de la junta directiva, 'admins'
        return $usuario->id_roles != 6;
    }

    protected function visor(IdentityInterface $usuario)
    {
        // id_roles 6 significa que eres un asociado que no puede gestionar la web
        return $usuario->id_roles === 6;
    }
}