<?php
namespace App\Policy;

use App\Model\Entity\Proveedor;
use Authorization\IdentityInterface;

class ProveedorPolicy
{
    public function canIndex(IdentityInterface $usuario, Proveedor $proveedor)
    {
        return $this->admin($usuario);
    }
    public function canInsertar(IdentityInterface $usuario, Proveedor $proveedor)
    {
        return $this->admin($usuario);
    }

    public function canEditar(IdentityInterface $usuario, Proveedor $proveedor) 
    {
        return $this->admin($usuario);
    }
    
    public function canEliminar(IdentityInterface $usuario, Proveedor $proveedor)
    {
        return $this->admin($usuario);
    }

    protected function admin(IdentityInterface $usuario)
    {
        // id_roles 1-5 son los cargos de la junta directiva, 'admins'
        return $usuario->id_roles != 6;
    }

    protected function visor(IdentityInterface $usuario)
    {
        // id_roles 6 significa que eres un asociado que no puede gestionar la web
        return $usuario->id_roles === 6;
    }
}