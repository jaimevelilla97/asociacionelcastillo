<?php
namespace App\Policy;

use App\Model\Entity\Actividad;
use Authorization\IdentityInterface;

class ActividadPolicy
{
    public function canInsertar(IdentityInterface $usuario, Actividad $actividad)
    {
        return $this->admin($usuario);
    }

    public function canEditar(IdentityInterface $usuario, Actividad $actividad)
    {
        return $this->admin($usuario);
    }
    
    public function canEliminar(IdentityInterface $usuario, Actividad $actividad)
    {
        return $this->admin($usuario);
    }
     
    protected function admin(IdentityInterface $usuario)
    {
        // id_roles 1-5 son los cargos de la junta directiva, 'admins'
        return $usuario->id_roles != 6;
    }

    protected function visor(IdentityInterface $usuario)
    {
        // id_roles 6 significa que eres un asociado que no puede gestionar la web
        return $usuario->id_roles === 6;
    }
}