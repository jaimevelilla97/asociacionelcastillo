<?php

namespace App\Controller;

class ProvinciasController extends AppController
{
    public function initialize(): void 
    {
        parent::initialize();

        $this->loadModel('Comunidades');
        $this->loadModel('Provincias');
    }

    public function getProvinciasByComunidad()
    {
        $this->Authorization->skipAuthorization();
        if ($this->request->is('post')) {
            $comunidad_id = (int) $this->request->getData("id");        
            $provincias = $this->Provincias->findByComunidadId($comunidad_id)->all();
        
            return $this->response
                ->withType('application/json')
                ->withStringBody(json_encode([
                'provincias' => $provincias
            ]));
        }
    }
}
?>