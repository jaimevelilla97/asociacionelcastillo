<?php

namespace App\Controller;

use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Event\EventInterface;

class BienvenidaController extends AppController
{
    public function initialize(): void 
    {
        parent::initialize();
        $this->loadModel('Usuarios');
    }

    public function index() 
    {
        $this->Authorization->skipAuthorization();
        $usuarios = $this->Usuarios->find()->all();
        
        // Coger de la base de datos el nombre completo de cada rol de la junta directiva
        foreach ($usuarios as $usuario) {
            $usuario_fullname = $usuario->nombre . " " . $usuario->apellidos;         
            if($usuario->id_roles == 1) {
                $presidente = $usuario_fullname;
            }else if($usuario->id_roles == 2) {
                $vicepresidente = $usuario_fullname;
            }else if($usuario->id_roles == 3) {
                $secretario = $usuario_fullname;
            }else if($usuario->id_roles == 4) {
                $tesorero = $usuario_fullname;
            }else if($usuario->id_roles == 5) {  
                $vocales[] = $usuario_fullname . " <br>";
            }
        }

        $this->set('presidente', $presidente);
        $this->set('vicepresidente', $vicepresidente);
        $this->set('tesorero', $tesorero);
        $this->set('secretario', $secretario);
        $this->set(compact('vocales'));
    }
}