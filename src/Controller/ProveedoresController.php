<?php

namespace App\Controller;

use Cake\Event\EventInterface;

class ProveedoresController extends AppController
{
    public function initialize(): void 
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
    }

    public function index() 
    {
        $this->Authorization->skipAuthorization();
        $proveedores = $this->Paginator->paginate($this->Proveedores->find());

        foreach ($proveedores as $proveedor) {
            //Convertir espacios en blanco a "desconocido" para ajustar tamaños de la tabla
            $email = $proveedor->email_contacto;
            $direccion = $proveedor->direccion;
            $web = $proveedor->web;
            $comentarios = $proveedor->comentarios;

            if ($email == "") {
                $email = "Desconocido";
            }
            if($direccion == "") {
                $direccion = "Desconocida";
            }
            if($web == "") {
                $web = "Desconocida";
            }
            if($comentarios == "") {
                $comentarios = "No hay";
            }
            $proveedor->email_contacto = $email;
            $proveedor->direccion = $direccion;
            $proveedor->web = $web;
            $proveedor->comentarios = $comentarios;
        }
        $this->set(compact('proveedores'));
    }
    
    public function insertar() 
    {
        $nuevo_proveedor = $this->Proveedores->newEmptyEntity();
        $this->Authorization->skipAuthorization();

        if($this->request->is('post')) {
            // Coger todos los campos que tienen los proveedores para guardar el nuevo proveedor
            $nuevo_proveedor = $this->Proveedores->patchEntity($nuevo_proveedor, $this->request->getData());
            
            if ($this->Proveedores->save($nuevo_proveedor)) {
                $this->Flash->success(__('El proveedor ha sido añadido exitosamente.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se ha podido añadir el proveedor.'));

            $errors = $nuevo_proveedor->errors;
            if ($errors) {
                foreach ($errors as $error) {
                    $this->Flash->error(__($error));
                }
            }
        }
        $this->set('proveedor', $nuevo_proveedor);
    }

    public function editar($id_proveedores) 
    {
        $proveedor = $this->Proveedores->get($id_proveedores);
        $this->Authorization->skipAuthorization();

        if ($this->request->is(['post', 'put'])) {
            $this->Proveedores->patchEntity($proveedor, $this->request->getData());
    
            if ($this->Proveedores->save($proveedor)) {
                $this->Flash->success(__("El proveedor ha sido editado exitosamente."));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__("No se ha podido editar el proveedor."));
            
            if ($proveedor->hasErrors()) {
                $errors = $proveedor->getErrors();
                foreach ($errors as $error) {
                    $this->Flash->error(__($error));
                }
            }
        }
        $this->set(compact('proveedor'));
    }

    public function eliminar($id_proveedores) 
    {        
        $this->request->allowMethod(['post', 'delete']);

        $proveedor = $this->Proveedores->get($id_proveedores);
        //$this->Authorization->authorize($proveedor);
        $this->Authorization->skipAuthorization();

        if ($this->Proveedores->delete($proveedor)) {
            $this->Flash->success(__("El proveedor ha sido eliminado exitosamente."));
            return $this->redirect(['action' => 'index']);
        }
    }
}