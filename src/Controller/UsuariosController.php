<?php

namespace App\Controller;

use Authentication\PasswordHasher\DefaultPasswordHasher;
use Cake\Event\EventInterface;

class UsuariosController extends AppController
{
    public function initialize(): void 
    {
        parent::initialize();
        $this->loadModel('Roles'); 
        $this->loadModel('Comunidades');
        $this->loadModel('Provincias');
        $this->loadModel('Localidades');

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['login']);
    }

    public function login() 
    {
        $this->Authorization->skipAuthorization();
        //var_dump((new DefaultPasswordHasher())->hash("1234"));
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();

        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            return $this->redirect('/');
        } else if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Introduzca un Usuario y Contraseña válidos por favor.'));
        }
        // display error if user submitted and authentication failed
    }

    public function logout()
    {
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Usuarios', 'action' => 'login']);
        }
    }

    public function index() 
    {
        $this->Authorization->skipAuthorization();

        $usuarios = $this->Paginator->paginate($this->Usuarios->find());
        foreach ($usuarios as $usuario) {
            $rol = $usuario->id_roles;
            if($rol > 0) {
                $rol = $this->Roles->get($rol)->nombre;
            }   
            $usuario->id_roles = $rol;
            
            // Convertir bit a Si o No en cuota_pagada
            $pagado = $usuario->cuota_pagada;
            if ($pagado == 0) {
            $pagado = "No";
            } elseif ($pagado == 1) {
            $pagado = "Sí";
            } 
            $usuario->cuota_pagada = $pagado;

            //Convertir espacios en blanco a "desconocido" para ajustar tamaños de la tabla
            $email = $usuario->email;
            if ($email == "") {
                $email = "Desconocido";
            }
            $usuario->email = $email;

            $apellidos = $usuario->apellidos;
            if ($apellidos ==  "") {
                $apellidos = "Desconocido";
            }
            $usuario->apellidos = $apellidos;
        }
        $this->set(compact('usuarios'));      
    }

    public function ver($id_usuarios) 
    {
        $usuario_actual_id = $this->request->getAttribute('identity')->id_usuarios;
        $usuario_actual = $this->Usuarios->get($usuario_actual_id);
        $this->Authorization->authorize($usuario_actual);

        $usuario = $this->Usuarios->get($id_usuarios);
        
        // Convertir el id de comunidad, provincia y localidad en texto (o en Desconocida  si el valor es NULL).
        $provincia_id = $usuario->provincia_id;
        if ($provincia_id > 0) {
            $provincia = $this->Provincias->findById($usuario->provincia_id)->firstOrFail();
            $usuario->provincia = $provincia->nombre;

            $comunidad = $this->Comunidades->findById($provincia->comunidad_id)->firstOrFail();
            $usuario->comunidad = $comunidad->nombre;
        } else {
            $usuario->provincia = "Desconocida";
            $usuario->comunidad = "Desconocida";
        }
        //Convertir el usuarios->id_roles en texto (roles->nombre)
        $rol = $usuario->id_roles;
        if($rol > 0) {
            $rol = $this->Roles->get($rol)->nombre;
        }   
        $usuario->id_roles = $rol;

         // Convertir bit a Si o No en cuota_pagada
        $pagado = $usuario->cuota_pagada;
         if ($pagado == 0) {
            $pagado = "No";
        } elseif ($pagado == 1) {
            $pagado = "Sí";
        } 
        $usuario->cuota_pagada = $pagado;

        //Convertir espacios en blanco a "desconocido" para ajustar tamaños de la tabla
        $email = $usuario->email;
        if ($email == "") {
            $email = "Desconocido";
        }
        $usuario->email = $email;
        $this->set(compact('usuario'));
    }

    public function insertar() 
    {
        $nuevo_usuario = $this->Usuarios->newEmptyEntity();
        $this->Authorization->authorize($nuevo_usuario);
                
        if ($this->request->is('post')) {
            $nuevo_usuario = $this->Usuarios->patchEntity($nuevo_usuario, $this->request->getData());

            $nuevo_usuario->cuota_pagada = (bool) (int) $nuevo_usuario->cuota_pagada;

            //encriptar con el Hasher la contraseña
            $nuevo_usuario->password = (new DefaultPasswordHasher())->hash($nuevo_usuario->password);
            
            // Verificar que ese nombre de usuario y dni NO existan, porque deben ser únicos
            $num_usuarios = $this->Usuarios->find()->where(["username" => $nuevo_usuario->username])->count(); 
            $num_dni = $this->Usuarios->find()->where(["dni" => $nuevo_usuario->dni])->count(); 

            if($num_usuarios == 0) {
                $validarUsername = true;
            } else {
                $validarUsername = false;
            }

            if($num_dni == 0) {
                $validarDni = true;
            } else {
                $validarDni = false;
            }

            $letra = substr($nuevo_usuario->dni, -1);
            $numeros = substr($nuevo_usuario->dni, 0, -1);
            // Validamos primero que el DNI no exista ya en otro usuario
            if ($validarDni == false) {
                $this->Flash->error(__('Ese DNI pertenece a otro usuario.'));
            } else { 
            // Validamos la fórmula del DNI
                if (( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8)){
                    if ($validarUsername == true) { 
                        if ($this->Usuarios->save($nuevo_usuario)) {
                            $this->Flash->success(__('El usuario ha sido añadido exitosamente.'));
                            return $this->redirect(['action' => 'index']);
                        } else {
                            $this->Flash->error(__('No se ha podido añadir el usuario. El DNI no era válido.'));
                        }   
                    } elseif ($validarUsername == false) {
                        $this->Flash->error(__('Error. El username ya existe.'));
                    }
                }
            }     
            $errors = $nuevo_usuario->errors;
            if ($errors) {
                foreach ($errors as $error) {
                    $this->Flash->error(__($error));
                }
            }
            // Convertir bit a Si o No en cuota_pagada 
            if ($usuario->cuota_pagada == "Si") {
                $usuario->cuota_pagada = 1;
            } elseif ($usuario->cuota_pagada = "No") {
                $usuario->cuota_pagada = 0;
            }
        }
       
        $roles_list = ['keyField' => 'id_roles', 'valueField' => 'nombre'];
        $roles = $this->Roles->find('list', $roles_list)->all()->toArray();
        $this->set(compact('roles'));
        
        $select_list = ['keyField' => 'id', 'valueField' => 'nombre'];
        $comunidades = $this->Comunidades->find('list', $select_list)->all()->toArray();
        $this->set(['comunidades' => $comunidades]);

        $this->set('usuario', $nuevo_usuario);
    }

    public function editar($id_usuarios) 
    {
        $usuario = $this->Usuarios->get($id_usuarios);
        $this->Authorization->skipAuthorization();


        $antiguaPassword = $usuario->password;
        $antiguoUsername = $usuario->username;
        $antiguoDni = $usuario->dni;

        if ($this->request->is(['post', 'put'])) {
            $this->Usuarios->patchEntity($usuario, $this->request->getData());
            $usuario->cuota_pagada = (bool) (int) $usuario->cuota_pagada;

            // Convertir bit a Si o No en cuota_pagada 
            if ($usuario->cuota_pagada == "Si") {
                $usuario->cuota_pagada = 1;
            } elseif ($usuario->cuota_pagada = "No") {
                $usuario->cuota_pagada = 0;
            }

            // Codificar la contraseña en la BBDD
            $nuevaPassword = $usuario->password;
            if (empty($nuevaPassword)) {
                $usuario->setDirty('password', false);
            } else {
                $usuario->password = (new DefaultPasswordHasher())->hash($usuario->password);
            }

            // Verificar que ese nombre de usuario NO exista, para que sea único
            $num_usuarios = $this->Usuarios->find()->where(["username" => $usuario->username])->count();
            $num_dni = $this->Usuarios->find()->where(["dni" => $usuario->dni])->count();
            
            if(($num_usuarios == 0) || ($num_usuarios == 1 && $usuario->username == $antiguoUsername)) {
                $validarUsername = true;
            } else {
                $validarUsername = false;
            }
            
            if(($num_dni == 0) || ($num_dni == 1 && $usuario->dni == $antiguoDni)) {
                $validarDni = true;
            } else {
                $validarDni = false;
            }

            // Validar la fórmula del DNI
            $letra = substr($usuario->dni, -1);
            $numeros = substr($usuario->dni, 0, -1);

            // Confirmamos primero si existe o no el DNI (es único).
            if ($validarDni == false) { 
                $this->Flash->error(__('Ese DNI pertenece a otro usuario.'));

            } else {
                if ((substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ) && $validarUsername == true) {
                    if ($this->Usuarios->save($usuario)) {
                        $this->Flash->success(__('El usuario ha sido añadido exitosamente.'));
                        return $this->redirect(['action' => 'index']);
                    }
                } elseif ($validarUsername == false) {
                    $this->Flash->error(__('El Username ya existe.'));
                } else {
                    $this->Flash->error(__('No se ha podido añadir el usuario. El DNI no era válido.'));
                }       
                 
                if ($usuario->hasErrors()) {
                    $errors = $usuario->getErrors();
                    foreach ($errors as $error) {
                        $this->Flash->error(__($error));
                    }
                }
            }
            
        }
        $roles_list = ['keyField' => 'id_roles', 'valueField' => 'nombre'];
        $roles = $this->Roles->find('list', $roles_list)->all()->toArray();
        $this->set(compact('roles'));

        $comunidades_list = ['keyField' => 'id', 'valueField' => 'nombre'];
        $comunidades = $this->Comunidades->find('list', $comunidades_list)->all()->toArray();
        $this->set(compact('comunidades'));
        $provincias = $this->Provincias->find('list', $comunidades_list)->all()->toArray();
        $this->set(compact('comunidades'));  

        $this->set(compact('usuario'));
    }

    public function eliminar($id_usuarios) 
    {
        $this->request->allowMethod(['post', 'delete']);

        $usuario = $this->Usuarios->findByIdUsuarios($id_usuarios)->firstOrFail();
        $this->Authorization->authorize($usuario);

        if ($this->Usuarios->delete($usuario)) {
            $this->Flash->success(__("El usuario ha sido eliminado exitosamente."));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function search()
    {
        $this->Authorization->skipAuthorization();
        $usuarios = $this->Usuarios->find()->all();

        $nombres = [];
        $apellidos = [];
        foreach ($usuarios as $usuario) {
            //quitar espacios en blanco antes y después del nombre, por si alguien escribe mal y se guarda en la tabla
            $usuario_nombre = trim($usuario->nombre); 
            $usuario_apellidos = trim($usuario->apellidos); 
            //hacerlo unico sin que se repitan 8 Pepes
            if (!in_array($usuario_nombre, $nombres)) $nombres[] = $usuario_nombre; 
            if (!in_array($usuario_apellidos, $apellidos)) $apellidos[] = $usuario_apellidos;
        }

        $this->set(compact('nombres'));
        $this->set(compact('apellidos'));

        if ($this->request->is(['post'])) {
            $options = array(
                'nombre' => $this->request->getData('searchNombre'),
                'apellidos' => $this->request->getData('searchApellidos'),
                'email' => $this->request->getData('searchEmail'),
                'telefono' => $this->request->getData('searchTelefono'),
                'dni' => $this->request->getData('searchDNI'),
                'direccion' => $this->request->getData('searchDireccion'),
            );

            $cond = '';
            $noopt = true;
            $query = $this->Usuarios->find();
            foreach ($options as $column => $value) {
                if (!empty($value)) {
                    $noopt = false;
                    //consulta por 'contener ese string', no por 'empezar'. Útil para nombres compuestos por ejemplo.
                    $query->where(["$column LIKE" => "%$value%"]); 
                }
            }
            $usuarios = $query;
           
            $this->set(compact('usuarios'));
            $this->set('search', true); // para que solo ponga el paginator en index y no en search
            foreach ($usuarios as $usuario) {                
                // Convertir bit a Si o No en cuota_pagada
                $pagado = $usuario->cuota_pagada;
                if ($pagado == 0) {
                $pagado = "No";
                } elseif ($pagado == 1) {
                $pagado = "Sí";
                } 
                $usuario->cuota_pagada = $pagado;
            }
            return $this->render('/Usuarios/index');  //nos carga el index en el /search con las condiciones de búsqueda
        }
    }

    public function sortear() {
        $this->Authorization->skipAuthorization();
        
        //$participantes = $this->Usuarios->find()->where("id_usuarios")->count();

        $ganador = $this->Usuarios->find()->select(['id_usuarios', 'nombre', 'apellidos'])->order('rand()')->first();
        $anuncio = ("El ganador del sorteo es: <b>" . $ganador->nombre . " " . $ganador->apellidos . "</b> cuyo ID identificador 
        de socio es: <b>" . $ganador->id_usuarios ."</b>");
        $this->set('anuncio', $anuncio);
    }
}