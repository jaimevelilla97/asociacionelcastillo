<?php

namespace App\Controller;

use Cake\Event\EventInterface;

class ActividadesController extends AppController
{

    public function initialize(): void 
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');

    }
    public function index() 
    {
        $this->Authorization->skipAuthorization();
        $actividades = $this->Paginator->paginate($this->Actividades->find());

        foreach ($actividades as $actividad) {
            // Convertir duración en minutos a dias/h/min
            $min = $actividad->duracion;
            $sec = $min * 60;
            $dias=floor($sec/86400);
            $mod_hora=$sec%86400;               //mod_hora es el sobrante, en horas, de la division de días; 
            $horas=floor($mod_hora/3600);       //hora es la division entre el sobrante de horas y 3600 segundos que representa una hora;
            $mod_minuto=$mod_hora%3600;         //mod_minuto es el sobrante, en minutos, de la division de horas; 
            $minutos=floor($mod_minuto/60);     //minuto es la division entre el sobrante y 60 segundos que representa un minuto;
            if($horas<=0) {
                $text = $minutos.' min';
            }
            elseif($dias<=0) { 
                if($mod_hora == 0) {
                    $text = $horas.' h';
                } else {
                    $text = $horas." h ".$minutos ." min";
                }
            } else {
                if($mod_minuto == 0) {
                    $text = $dias.' dias';
                } else {
                    $text = $dias." dias ".$horas." hrs ".$minutos." min";
                }
            }
            if ($min == 0) {
                $text = "Desconocida";
            }
            $actividad->duracion = $text; 
            
            // Convertir el valor numérico precio en € para la vista
            $euros = $actividad->precio;
            if($euros > 0) {
                $actividad->precio = $euros . " €";
            } else {
                $actividad->precio = "Gratis";
            }

            //Convertir espacios en blanco a "desconocido" para ajustar tamaños de la tabla
            $email = $actividad->email_contacto;
            if ($email == "") {
                $email = "Desconocido";
            }
            $actividad->email_contacto = $email;

            $descripcion = $actividad->descripcion;
            if ($descripcion ==  "") {
                $descripcion = "Desconocida";
            }
            $actividad->descripcion = $descripcion;

            $tlf = $actividad->telefono_contacto;
            if ($tlf ==  "") {
                $tlf = "Desconocido";
            }
            $actividad->telefono_contacto = $tlf;
        } 
        $this->set(compact('actividades'));
    }
    
    public function insertar() 
    {
        $nueva_actividad = $this->Actividades->newEmptyEntity();
        $this->Authorization->authorize($nueva_actividad);
        //$this->Authorization->skipAuthorization();

        if($this->request->is('post')) {
            $nueva_actividad = $this->Actividades->patchEntity($nueva_actividad, $this->request->getData());
            $nueva_actividad->hora = $this->request->getData('hora');

            if($nueva_actividad->nombre === '') {
                $nueva_actividad->setError("nombre", __("El nombre es un campo requerido"));
            }
            if($nueva_actividad->fecha === '') {
                $nueva_actividad->setError("descripcion", __("La descripcion es un campo requerido"));
            }

            if ($this->Actividades->save($nueva_actividad)) {
                $this->Flash->success(__('La actividad ha sido añadida exitosamente.'));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('No se ha podido añadir la actividad.'));

            $errors = $nueva_actividad->errors;
            if ($errors) {
                foreach ($errors as $error) {
                    $this->Flash->error(__($error));
                }
            }
        }
        $this->set('actividad', $nueva_actividad);
    }

    public function editar($id_actividades) 
    {
        $actividad = $this->Actividades->findByIdActividades($id_actividades)->firstOrFail();
        $this->Authorization->authorize($actividad);
        $this->set(compact('actividad'));

        if ($this->request->is(['post', 'put'])) {
            $this->Actividades->patchEntity($actividad, $this->request->getData());
            $actividad->hora = $this->request->getData('hora');

            if ($this->Actividades->save($actividad)) {
                $this->Flash->success(__("La actividad ha sido editada exitosamente."));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__("No se ha podido editar la actividad"));
            
            if ($actividad->hasErrors()) {
                $errors = $actividad->getErrors();
                foreach ($errors as $error) {
                    $this->Flash->error(__($error));
                }
            }
        }
        $this->set(compact('actividad'));
    }

    public function eliminar($id_actividades) 
    {
        $this->request->allowMethod(['post', 'delete']);

        $actividad = $this->Actividades->findByIdActividades($id_actividades)->firstOrFail();
        $this->Authorization->authorize($actividad);

        if ($this->Actividades->delete($actividad)) {
            $this->Flash->success(__("La actividad ha sido eliminada exitosamente."));
            return $this->redirect(['action' => 'index']);
        }
    }
}