<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Rule\IsUnique;

class UsuariosTable extends Table
{
    public function initialize(array $config): void
    {
        /* Abrimos posibilidad de en un futuro controlar quién inserta y edita usuarios y a qué fecha/hora para registrar un log.
         * Sólo si la junta directiva lo requiere, implementaremos ésto. Para ello usaremos la siguiente función de cakephp.
            $this->addBehavior('Timestamp', [
                'events' => [
                'Model.beforeSave' => [
                    'created_at' => 'new',
                    'updated_at' => 'always',
                ],
                'Orders.completed' => [
                    'completed_at' => 'always'
                ]
            ]
        ]); 
        */
    }
    
    // Validación de los campos que queremos como obligatorios, vacíos o sin requerimiento en el 'Insertar'.
    public function validationInsertar(Validator $validator): Validator
    {            
        $msg = "Por favor escriba su ";
        $validator
            ->requirePresence('nombre',  $msg . 'nombre.')
            ->notEmptyString('nombre', $msg . 'nombre.')
            ->maxLength('nombre', 255)

            ->requirePresence('apellido',  $msg . 'apellido.')
            ->notEmptyString('apellido',  $msg . 'nombre.')
            ->maxLength('apellido', 255)

            ->requirePresence('dni',  $msg . 'DNI (9 caracteres).')
            ->maxLength('dni', 9)

            ->requirePresence('id_roles',  $msg . 'cargo.')
            ->greaterThan('id_roles', 0,  $msg . 'cargo.')

            ->requirePresence('username',  $msg . 'nombre de usuario.')
            ->notEmptyString('username',  $msg . 'nombre de usuario.')
            ->maxLength('username', 255)
            
            ->requirePresence('password', $msg . 'contraseña.')
            ->minLength('password', 8, 'La contraseña debe tener más de 8 caracteres.')
            ->maxLength('password', 255)

            // Campos no requeridos (pueden estar vacíos)
            ->allowEmptyString('email', true) // la gente muy mayor no suele tener email 
            ->allowEmptyString('telefono', true)
            ->allowEmptyString('fecha_nacimiento', true)
            ->allowEmptyString('comunidad', true)
            ->allowEmptyString('provincia', true)
            ->allowEmptyString('direccion', true);

        return $validator;
    }
    // Validación de los campos que queremos como obligatorios, vacíos o sin requerimiento en el 'Editar'.
    public function validationEditar(Validator $validator): Validator
    {
        $msg = "Por favor escriba su ";
        $validator
            ->requirePresence('nombre',  $msg . 'nombre.')
            ->notEmptyString('nombre', $msg . 'nombre.')
            ->maxLength('nombre', 255)

            ->requirePresence('apellido',  $msg . 'apellido.')
            ->notEmptyString('apellido',  $msg . 'nombre.')
            ->maxLength('apellido', 255)

            ->requirePresence('dni',  $msg . 'DNI (8 letras 1 número).')
            ->maxLength('dni', 9)

            ->requirePresence('id_roles',  $msg . 'cargo.')
            ->greaterThan('id_roles', 0,  $msg . 'cargo.')

            ->requirePresence('username',  $msg . 'nombre de usuario.')
            ->notEmptyString('username',  $msg . 'nombre de usuario.')
            ->maxLength('username', 255)


            // Permitirlas vacías (la contraseña porque si está vacía mantiene la antigua, que es obligada en insertar)
            ->allowEmptyString('password', true)
            ->allowEmptyString('telefono', true)
            ->allowEmptyString('fecha_nacimiento', true)
            ->allowEmptyString('comunidad', true)
            ->allowEmptyString('provincia', true)
            ->allowEmptyString('direccion', true)
            ->allowEmptyString('email', true);

        return $validator;
    }
}