<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ActividadesTable extends Table
{
    public function initialize(array $config): void
    {
    }

    public function validationInsertar(Validator $validator): Validator
    {            
        $msg_nombre = 'Por favor escriba su nombre.';
        $validator
            ->requirePresence('nombre', $msg_nombre)
            ->notEmptyString('nombre', $msg_nombre)
            ->maxLength('nombre', 255)

            // Campos no requeridos (pueden estar vacíos)
            ->allowEmptyString('email', true) // la gente muy mayor no suele tener email 
            ->allowEmptyString('telefono', true)
            ->allowEmptyString('direccion', true);

        return $validator;
    }

    public function validationEditar(Validator $validator): Validator
    {
        $msg_nombre = 'Por favor escriba su nombre.';

        $validator
            ->requirePresence('nombre', $msg_nombre)
            ->notEmptyString('nombre', $msg_nombre)
            ->maxLength('nombre', 255)

            // Permitirlas vacías
            ->allowEmptyString('telefono', true)
            ->allowEmptyString('direccion', true)
            ->allowEmptyString('email', true);

        return $validator;
    }
}