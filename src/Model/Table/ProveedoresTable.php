<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProveedoresTable extends Table
{
    public function initialize(array $config): void
    {
    }

    public function validationInsertar(Validator $validator): Validator
    {            
        $msg_nombre = 'Por favor escriba su nombre.';

        $validator
            ->requirePresence('nombre_completo', $msg_nombre)
            ->notEmptyString('nombre_completo',  $msg_nombre);
        return $validator;
    }

    public function validationEditar(Validator $validator): Validator
    {
        $msg_nombre = 'Por favor escriba su nombre.';

        $validator
            ->requirePresence('nombre_completo', $msg_nombre)
            ->notEmptyString('nombre_completo',  $msg_nombre);
        return $validator;
    }
}