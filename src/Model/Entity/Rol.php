<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Rol extends Entity
{
    protected $_accessible = [
        '*'           => true,
        'id_roles'    => false,
        'nombre'      => false,
    ];
}