<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Actividad extends Entity
{
    protected $_accessible = [
        '*'                 => false,
        'id_actividades'    => true,
        'nombre'            => true,
        'fecha'             => true,
        'hora'              => true,
        'duracion'          => true,
        'precio'            => true,
        'email_contacto'    => true,
        'telefono_contacto' => true,
        'descripcion'       => true,
    ];
}