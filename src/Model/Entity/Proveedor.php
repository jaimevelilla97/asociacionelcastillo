<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Proveedor extends Entity
{
    protected $_accessible = [
        '*'                 => false,
        'id_proveedores'    => true,
        'nombre_completo'   => true,
        'telefono'          => true,
        'email'             => true,
        'direccion'         => true,
        'web'               => true,
        'comentarios'       => true,
    ];
}