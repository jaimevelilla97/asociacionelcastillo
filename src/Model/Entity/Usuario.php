<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Usuario extends Entity
{
    protected $_accessible = [
        '*'                 => false,
        'id_usuarios'       => true,
        'id_roles'          => true,
        'nombre'            => true,
        'apellidos'         => true,
        'fecha_nacimiento'  => true,
        'username'          => true,
        'password'          => true,
        'dni'               => true,
        'telefono'          => true,
        'email'             => true,
        'direccion'         => true,
        'cuota_pagada'      => true,
        'comunidad_id'      => true,
        'provincia_id'      => true,
    ];
}